#pragma once
#include<array>
#include<vector>
#include<algorithm>

using namespace std;

const bool DESCENDING = false,
ASCENDING = true;

void RankingSort(vector<int>& numbers)
{
	vector<int> rank;
	vector<int> sorted;
	sorted.resize(numbers.size());
	rank.resize(numbers.size());

	for (int index1 = 0; index1 < numbers.size(); ++index1)
		for (int index2 = 0; index2 < index1; ++index2)
			if (numbers[index1] > numbers[index2])
				rank[index1]++;
			else
				rank[index2]++;

	for (int index = 0; index < numbers.size(); ++index)
		sorted[rank[index]] = numbers[index];

	numbers = sorted;
}

void BubbleSort(vector<int>& vector)
{
	for (int index1 = 0; index1 < vector.size(); ++index1)
		for (int index2 = 0; index2 < vector.size() - index1 - 1; ++index2)
		{
			int position1 = index2,
			position2 = index2 + 1;

			if (vector[position1] > vector[position2])
			{
				int temp = vector[position1];
				vector[position1] = vector[position2];
				vector[position2] = temp;
			}
		}
}

bool BitonicCompare(int first, int second, bool direction)
{
	return direction == (first > second);
}

void BitonicSorting	(vector<int>& vector, int length, int start, bool direction)
{
	if (length <= 1)
		return;

	int half = length / 2;

	for (int index = start; index < start + half; ++index)
	{
		int secondPos = index + half;
		if (BitonicCompare(vector[index], vector[secondPos], direction))
		{
			int temp = vector[index];
			vector[index] = vector[secondPos];
			vector[secondPos] = temp;
		}
	}

	BitonicSorting(vector, half, start, direction);
	BitonicSorting(vector, half, start + half, direction);
}

void BitonicSort(vector<int>& vector, int length, int start = 0, bool direction = ASCENDING)
{
	if (length <= 1)
		return;

	int half = length / 2;

	BitonicSort(vector, half, start, ASCENDING);
	BitonicSort(vector, half, start + half, DESCENDING);

	BitonicSorting(vector, length, start, direction);
}

void Merge(vector<int>& vector, int start, int middle, int end)
{
	std::vector<int> first, second;
	first.resize(middle - start + 1);
	second.resize(end - middle);

	for (int index = 0; index < first.capacity(); index++)
		first[index] = vector[start + index];
	for (int index = 0; index < second.capacity(); index++)
		second[index] = vector[middle + 1 + index];

	int index1 = 0, index2 = 0, indexFinal = start;
	while (index1 < first.capacity() && index2 < second.capacity())
	{
		if (first[index1] <= second[index2])
		{
			vector[indexFinal] = first[index1];
			index1++;
		}
		else
		{
			vector[indexFinal] = second[index2];
			index2++;
		}
		indexFinal++;
	}

	while (index1 < first.capacity())
	{
		vector[indexFinal] = first[index1];
		index1++;
		indexFinal++;
	}

	while (index2 < second.capacity())
	{
		vector[indexFinal] = second[index2];
		index2++;
		indexFinal++;
	}
}


void MergeSort(vector<int>& vector, int start, int end)
{
	if (start >= end)
		return;

	int middle = (start + end) / 2;

	MergeSort(vector, start, middle);
	MergeSort(vector, middle + 1, end);
	Merge(vector, start, middle, end);
}

void LinearSort(vector<int>& vector)
{
	int min = abs(*min_element(vector.begin(), vector.end())), max = abs(*max_element(vector.begin(), vector.end()));

	int capacity = (max > min) ? max : min;
	std::vector<int> counts;
	counts.resize(capacity * 2 + 1);

	for (int index = 0; index < vector.size(); ++index)
	{
		counts[vector[index] + capacity]++;
	}

	int count = 0;

	for (int index1 = 0; index1 < capacity * 2 + 1; ++index1)
	{
		for (int index2 = 0; index2 < counts[index1]; ++index2)
		{
			vector[count++] = index1 - capacity;
		}
	}
}