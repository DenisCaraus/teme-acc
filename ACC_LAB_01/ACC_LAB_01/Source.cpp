#include<iostream>
#include<random>
#include <chrono>
#include"Sorting.h"

using namespace std::chrono;

template<int size>
void generateVector(std::vector<int>& vector)
{
	static const int min = -10000000, max = 10000000;
	vector.resize(size);
	std::random_device device;
	std::mt19937 generator(device());

	std::uniform_int_distribution<int> distribution(min, max);

	std::generate(vector.begin(), vector.end(), [&]() { return distribution(generator); });
}

int main()
{
	/* Correctitude tests: */ 

	std::cout << "Teste de corectitudine: " << std::endl;
	std::cout << "Vector initial:" <<std::endl;
	std::vector<int> vector = { -10000000, 7, 4, 8, 6, 2, 1, 10000000, -10000000, 7, 4, 8, 6, 2, 1, 10000000 };
	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl << "Bubble sort :" <<std::endl;

	BubbleSort(vector);

	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl << "Ranking sort :" << std::endl;
	vector = { -10000000, 7, 4, 8, 6, 2, 1, 10000000, -10000000, 7, 4, 8, 6, 2, 1, 10000000 };

	RankingSort(vector);

	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl << "Bitonic sort :" << std::endl;
	vector = { -10000000, 7, 4, 8, 6, 2, 1, 10000000, -10000000, 7, 4, 8, 6, 2, 1, 10000000 };

	BitonicSort(vector, vector.size());

	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl << "Merge sort :" << std::endl;
	vector = { -10000000, 7, 4, 8, 6, 2, 1, 10000000, -10000000, 7, 4, 8, 6, 2, 1, 10000000 };

	MergeSort(vector, 0, vector.size() - 1);

	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl << "Linear sort :" << std::endl;
	vector = { -10000000, 7, 4, 8, 6, 2, 1, 10000000, -10000000, 7, 4, 8, 6, 2, 1, 10000000 };

	LinearSort(vector);

	std::copy(vector.begin(), vector.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;
	

	// Speed calculation

	std::cout << "Teste de viteza: " << std::endl;

	std::vector<int> original1, sorted1,
		original2, sorted2,
		original3, sorted3,
		original4, sorted4,
		bitonic1,
		bitonic2,
		bitonic3,
		bitonic4;

	generateVector<100000>(original1); sorted1 = original1;
	generateVector<300000>(original2); sorted2 = original2;
	generateVector<600000>(original3); sorted3 = original3;
	generateVector<1000000>(original4); sorted4 = original4;

	generateVector<131072>(bitonic1);
	generateVector<262144>(bitonic2);
	generateVector<524288>(bitonic3);
	generateVector<1048576>(bitonic4);
/*
	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		BubbleSort(sorted1);
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Bubble sort 100000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BubbleSort(sorted2);
		timeEnd = high_resolution_clock::now();
		std::cout << "Bubble sort 300000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BubbleSort(sorted3);
		timeEnd = high_resolution_clock::now();
		std::cout << "Bubble sort 600000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BubbleSort(sorted4);
		timeEnd = high_resolution_clock::now();
		std::cout << "Bubble sort 1000000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}
	
	sorted1 = original1;
	sorted2 = original2;
	sorted3 = original3;
	sorted4 = original4;

	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		RankingSort(sorted1);
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Ranking sort 100000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		RankingSort(sorted2);
		timeEnd = high_resolution_clock::now();
		std::cout << "Ranking sort 300000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		RankingSort(sorted3);
		timeEnd = high_resolution_clock::now();
		std::cout << "Ranking sort 600000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		RankingSort(sorted4);
		timeEnd = high_resolution_clock::now();
		std::cout << "Ranking sort 1000000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}

	sorted1 = original1;
	sorted2 = original2;
	sorted3 = original3;
	sorted4 = original4;
	*/
	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		MergeSort(sorted1, 0, sorted1.size() - 1);
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Merge sort 100000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		MergeSort(sorted2, 0, sorted2.size() - 1);
		timeEnd = high_resolution_clock::now();
		std::cout << "Merge sort 300000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		MergeSort(sorted3, 0, sorted3.size() - 1);
		timeEnd = high_resolution_clock::now();
		std::cout << "Merge sort 600000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		MergeSort(sorted4, 0, sorted4.size() - 1);
		timeEnd = high_resolution_clock::now();
		std::cout << "Merge sort 1000000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}

	sorted1 = original1;
	sorted2 = original2;
	sorted3 = original3;
	sorted4 = original4;

	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		BitonicSort(sorted1, sorted1.size());
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 100000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(sorted2, sorted2.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 300000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(sorted3, sorted3.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 600000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(sorted4, sorted4.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 1000000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}


	sorted1 = original1;
	sorted2 = original2;
	sorted3 = original3;
	sorted4 = original4;

	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		BitonicSort(bitonic1, bitonic1.size());
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 131072: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(bitonic2, bitonic2.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 262144: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(bitonic3, bitonic3.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 524288: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		BitonicSort(bitonic4, bitonic4.size());
		timeEnd = high_resolution_clock::now();
		std::cout << "Bitonic sort 1048576: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}
	
	{
		high_resolution_clock::time_point timeStart = high_resolution_clock::now();
		LinearSort(sorted1);
		high_resolution_clock::time_point timeEnd = high_resolution_clock::now();
		std::cout << "Linear sort 100000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		LinearSort(sorted2);
		timeEnd = high_resolution_clock::now();
		std::cout << "Linear sort 300000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		LinearSort(sorted3);
		timeEnd = high_resolution_clock::now();
		std::cout << "Linear sort 600000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

		timeStart = high_resolution_clock::now();
		LinearSort(sorted4);
		timeEnd = high_resolution_clock::now();
		std::cout << "Linear sort 1000000: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;
	}

	return 0;
}