#include "Graf.h"

int Graf::costEstimate(int start, int goal)
{
	int x1 = nodeList[start].first,
		x2 = nodeList[goal].first,
		y1 = nodeList[start].second,
		y2 = nodeList[goal].second;

	return sqrt(pow((x2 - x1), 2) + pow((y2 - y1), 2));
}

std::map<int, int> GraphMap(const Graf& graf, int value = 0)
{
	std::map<int, int> map;
	for (int index = 0; index < graf.nodeList.size(); ++index)
	{
		map.emplace(index, value);
	}
	return map;
}

int minimumEstimatedDistance(const std::vector<int>& list, std::map<int, int>& estimatedCost)
{
	if (list.empty())
		return -1;

	int min = INT_MAX,
		minPos = -1;

	for (int index = 0; index < list.size(); ++index)
	{
		int node = list[index];
		if (estimatedCost[node] <= min)
		{
			min = estimatedCost[node];
			minPos = index;
		}
	}

	return minPos;
}

int minimumEstimatedDistanceVector(const std::vector<int>& list, std::vector<int>& estimatedCost)
{
	if (list.empty())
		return -1;

	int min = INT_MAX,
		minPos = -1;

	for (int index = 0; index < list.size(); ++index)
	{
		int node = list[index];
		if (estimatedCost[node] <= min)
		{
			min = estimatedCost[node];
			minPos = index;
		}
	}

	return minPos;
}

int minimumDistance(const std::vector<int>& distance, const std::vector<bool>& W)
{
	int min = INT_MAX, id;
	for (int index = 0; index < W.size(); ++index)
	{
		if (W[index] == true)
		{
			if (distance[index] <= min)
			{
				min = distance[index];
				id = index;
			}
		}
	}
	return id;
}

Graf::Graf()
{
	int nodeCount, id, latitude, longitude, length = 0, idNext;
	tinyxml2::XMLDocument file;
	file.LoadFile("map2.xml");
	if (file.ErrorID() == 0)
	{
		nodeCount = file.FirstChildElement("map")->FirstChildElement("nodes")->LastChildElement("node")->IntAttribute("id") + 1;
		adjacencyLists.resize(nodeCount);

		tinyxml2::XMLElement *pArc, *pNode;

		pNode = file.FirstChildElement("map")->FirstChildElement("nodes")->FirstChildElement("node");
		pArc = file.FirstChildElement("map")->FirstChildElement("arcs")->FirstChildElement("arc");
		while (pNode)
		{
			longitude = 0;
			latitude = 0;
			id = 0;
			
			id = pNode->IntAttribute("id");
			longitude = pNode->IntAttribute("longitude");
			latitude = pNode->IntAttribute("latitude");

			nodeList.push_back(std::make_pair(longitude, latitude));
			adjacencyLists[id].push_back(std::make_pair(id, length));
			pNode = pNode->NextSiblingElement("node");
		}
		while (pArc)
		{
			id = 0; 
			idNext = 0;

			id = pArc->IntAttribute("from");
			idNext = pArc->IntAttribute("to");
			length = pArc->IntAttribute("length");

			adjacencyLists[id].push_back(std::make_pair(idNext, length));
			pArc = pArc->NextSiblingElement("arc");
		}
	}
}

Graf::~Graf()
{
}

//std::vector<int> Graf::dijkstra(int start)
//{
//	std::vector<bool> W;
//	std::vector<int> distance;
//	std::vector<int> predecesor;	
//
//	W.resize(nodeList.size());
//	for (int index = 0; index < W.size(); ++index)
//	{
//		W[index] = true;
//	}
//
//	distance.resize(nodeList.size());
//	distance[start] = 0;
//	predecesor.resize(nodeList.size());
//	predecesor[start] = 0;
//
//	for (int index = 0; index<nodeList.size(); ++index)
//	{
//		if (index != start)
//		{
//			distance[index] = INT_MAX;
//			predecesor[index] = 0;
//		}
//	}
//
//	for (int count = 0; count < W.size(); ++count)
//	{
//		int x = minimumDistance(distance, W);
//		W[x] = false;
//		int adjSize = adjacencyLists[x].size();
//		for (int index = 0; index < adjSize; ++index)
//		{
//			std::pair<int, int> y = adjacencyLists[x][index];
//			if ((W[std::get<0>(y)] == true) && (distance[x] != INT_MAX))
//			{
//				distance[std::get<0>(y)] = distance[x] + std::get<1>(y);
//				predecesor[std::get<0>(y)] = x;
//			}
//		}
//	}
//
//	return predecesor;
//}

std::vector<int> Graf::dijkstra(int start)
{
	std::vector<bool> Visited;
	std::vector<int> distance;
	std::vector<int> predecesor;
	std::priority_queue<std::pair<int, int>, std::vector<std::pair<int, int>>, std::greater<std::pair<int, int>>> queue;

	distance.resize(nodeList.size());
	distance[start] = 0;
	predecesor.resize(nodeList.size());
	predecesor[start] = 0;

	for (int index = 0; index < nodeList.size(); ++index)
	{
		if (index != start)
		{
			distance[index] = INT_MAX;
			predecesor[index] = -1;
		}
	}

	Visited.resize(nodeList.size());

	queue.emplace(0, start);

	while(!queue.empty())
	{
		std::pair<int, int> x = queue.top();
		queue.pop();
		Visited[std::get<1>(x)] = true;
		int adjSize = adjacencyLists[std::get<1>(x)].size();
		for (int index = 1; index < adjSize; ++index)
		{
			std::pair<int, int> y = adjacencyLists[std::get<1>(x)][index];
			if (Visited[std::get<0>(y)] != true)
			{
				int newLength = distance[std::get<1>(x)] + std::get<1>(y);
				if (newLength < distance[std::get<0>(y)])
				{

					distance[std::get<0>(y)] = newLength;
					queue.emplace(newLength, std::get<0>(y));
					predecesor[std::get<0>(y)] = std::get<1>(x);
				}
			}
		}
	}

	return predecesor;
}

std::map<int, int> Graf::AStar(int start, int goal)
{
	std::vector<int> verified;
	std::vector<int> visited;

	visited.emplace_back(start);

	std::map<int, int> predecesor = GraphMap(*this);
	std::map<int, int> cost = GraphMap(*this, INT_MAX);
	std::map<int, int> estimatedTotalCost = GraphMap(*this, INT_MAX);

	cost[start] = 0;
	estimatedTotalCost[start] = this->costEstimate(start, goal);

	while (!visited.empty())
	{
		int visitedPos = minimumEstimatedDistance(visited, estimatedTotalCost);

		if (visitedPos == -1)
			break;

		int currentNode = visited[visitedPos];

		if (currentNode == goal)
			break;

		verified.emplace_back(currentNode);
		visited.erase(visited.begin() + visitedPos);

		int adjSize = adjacencyLists[currentNode].size();
		for (std::pair<int, int> pair : adjacencyLists[currentNode])
		{
			if (std::find(verified.begin(), verified.end(), pair.first) != verified.end())
				continue;

			int costTemp = cost[currentNode] + pair.second;

			if (std::find(visited.begin(), visited.end(), pair.first) == visited.end())
				visited.emplace_back(pair.first);

			if (costTemp >= cost[pair.first])
				continue;

			predecesor[pair.first] = currentNode;
			cost[pair.first] = costTemp;
			estimatedTotalCost[pair.first] = this->costEstimate(pair.first, goal) + cost[pair.first]; 
		}
	}

	return predecesor;
}

std::vector<int> Graf::AStarVector(int start, int goal)
{
	std::vector<int> verified;
	std::vector<int> visited;

	visited.emplace_back(start);

	std::vector<int> predecesor;
	std::vector<int> cost;
	std::vector<int> estimatedTotalCost;

	predecesor.resize(nodeList.size());
	cost.resize(nodeList.size(), INT_MAX);
	estimatedTotalCost.resize(nodeList.size(), INT_MAX);

	cost[start] = 0;
	estimatedTotalCost[start] = this->costEstimate(start, goal);

	while (!visited.empty())
	{
		int visitedPos = minimumEstimatedDistanceVector(visited, estimatedTotalCost);

		if (visitedPos == -1)
			break;

		int currentNode = visited[visitedPos];

		if (currentNode == goal)
			break;

		verified.emplace_back(currentNode);
		visited.erase(visited.begin() + visitedPos);

		int adjSize = adjacencyLists[currentNode].size();
		for (std::pair<int, int> pair : adjacencyLists[currentNode])
		{
			if (std::find(verified.begin(), verified.end(), pair.first) != verified.end())
				continue;

			int costTemp = cost[currentNode] + pair.second;

			if (std::find(visited.begin(), visited.end(), pair.first) == visited.end())
				visited.emplace_back(pair.first);

			if (costTemp >= cost[pair.first])
				continue;

			predecesor[pair.first] = currentNode;
			cost[pair.first] = costTemp;
			estimatedTotalCost[pair.first] = this->costEstimate(pair.first, goal) + cost[pair.first];
		}
	}

	return predecesor;
}

