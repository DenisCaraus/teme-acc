#pragma once
#include<algorithm>
#include<vector>
#include<queue>
#include<map>
#include "tinyxml2.h"

class Graf
{
public:
	Graf();
	~Graf();
	std::vector<int> dijkstra(int start);
	std::map<int, int> AStar(int start, int goal);
	std::vector<int> AStarVector(int start, int goal);
	int costEstimate(int start, int goal);

public:
	std::vector<std::pair<int, int>> nodeList;
	std::vector<std::vector<std::pair<int, int>>> adjacencyLists;
};
