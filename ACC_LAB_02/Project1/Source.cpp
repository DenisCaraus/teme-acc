#include"Graf.h"
#include<iostream>
#include<iterator>
#include<chrono>

using namespace std::chrono;

template<class T>
std::vector<int> GetPredecesor(T& predecesor)
{
	std::vector<int> pred;

	int last = predecesor[39750];
	pred.emplace_back(39750);
	while (last != 0)
	{
		pred.emplace_back(last);
		last = predecesor[last];
	}
	pred.emplace_back(0);

	return pred;
}

int main()
{
	Graf graf;

	high_resolution_clock::time_point timeStart = high_resolution_clock::now();
	std::vector<int> dijkstra = graf.dijkstra(0);
	high_resolution_clock::time_point timeEnd = high_resolution_clock::now();

	std::cout << "Dijkstra time: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

	timeStart = high_resolution_clock::now();
	std::map<int, int> aStar = graf.AStar(0, 39750);
	timeEnd = high_resolution_clock::now();

	std::cout << "A* map time: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

	timeStart = high_resolution_clock::now();
	std::vector<int> aStarVector = graf.AStarVector(0, 39750);
	timeEnd = high_resolution_clock::now();

	std::cout << "A* vector time: " << duration_cast<duration<double>>(timeEnd - timeStart).count() << " secunde." << std::endl;

	std::vector<int> predecesor1 = GetPredecesor(dijkstra),
		predecesor2 = GetPredecesor(aStar),
		predecesor3 = GetPredecesor(aStarVector);

	std::cout << "Dijkstra route: ";
	std::copy(predecesor1.begin(), predecesor1.end(), std::ostream_iterator<int>(std::cout, " "));

	std::cout << std::endl << std::endl << "A* map route: ";
	std::copy(predecesor2.begin(), predecesor2.end(), std::ostream_iterator<int>(std::cout, " "));

	std::cout << std::endl << std::endl << "A* vector route: ";
	std::copy(predecesor3.begin(), predecesor3.end(), std::ostream_iterator<int>(std::cout, " "));

	std::cout << std::endl;

	return 0;
}